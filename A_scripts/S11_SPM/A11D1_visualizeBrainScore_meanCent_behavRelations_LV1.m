%% Plot brainscores +- CI outside PLS

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data//mri/task/analyses/D_PLS_Dim/';
pn.functions    = [pn.root, 'T_tools/']; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]);

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools')
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools/RainCloudPlots'))

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult.mat'])

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

% N = 42 YA, 1125, 1214 excluded: no EEG data; 1151,1252,1228,1247 excluded before, not here
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:numel(groupsizes)
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1); % vsc: designscore
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1); % usc: brainscore
    end
end

%% assess relation of linear 1234 changes

individualBrainScores = uData{indGroup};

X = [1 1; 1 2; 1 3; 1 4];
b=X\individualBrainScores(1:4,:);
individualBrainScores_slope(:,1) = b(2,:);

%individualBrainScores_slope_win = winsor(individualBrainScores_slope,[10,90]);

%% get summary data

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

idx_AttFactor = find(ismember(STSWD_summary.IDs, IDs));
idx_BS = find(ismember(IDs,STSWD_summary.IDs));
[STSWD_summary.IDs(idx_AttFactor), IDs(idx_BS)]

%% plot relations

h = figure('units','normalized','position',[.1 .1 .3 .2]);
x = individualBrainScores_slope(idx_BS);
% add AMF
ax{1} = subplot(2,3,1); cla; hold on;
    y = STSWD_summary.EEG_LV1.slope_win(idx_AttFactor,1);
    %y = mean(STSWD_summary.EEG_LV1.data(idx_AttFactor,2:4),2)-STSWD_summary.EEG_LV1.data(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x,y)
    ylabel({'spectral BS';'(linear modulation)'})
% add drift
ax{2} = subplot(2,3,2); hold on;
    y = STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x,y)
    ylabel({'Drift Load 1'})
% add drift change
ax{3} = subplot(2,3,3); hold on;
    y = STSWD_summary.HDDM_vt.driftMRI_linear(idx_AttFactor,1);
    scatter(x,y, 'filled', 'MarkerFaceColor', [.5 1 1]);
    [r, p] = corrcoef(x,y)
    ylabel({'Drift';'(linear modulation)'})
% add 1/f
ax{4} = subplot(2,3,4); hold on;
    y = STSWD_summary.OneFslope.linear_win(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x,y)
    ylabel({'1/f slope';'(linear modulation)'})
% add entropy
ax{5} = subplot(2,3,5); hold on;
    y = STSWD_summary.SE.data_slope_win(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x, y)
    ylabel({'entropy';'(linear modulation)'})
% add pupildiff
ax{6} = subplot(2,3,6); hold on;
    y = STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1);
    scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
    [r, p] = corrcoef(x, y)
    ylabel({'pupil';'(linear modulation)'})
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

% make background black, axes labels white
set(h,'Color','k')
for indAx = 1:6; ax{indAx}.Color = [0 0 0]; ax{indAx}.XColor = [1 1 1]; ...
        ax{indAx}.YColor = [1 1 1]; end

%% plot signals of less interest

h = figure('units','normalized','position',[.1 .1 .3 .25/2]);
% add AMF
x = individualBrainScores_slope(idx_BS);
ax{1} = subplot(1,3,1); hold on;
    y = STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y)
    %title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(2,1),2)),',',num2str(round(correlationCI(2,2),2)),']'])
    ylabel({'Non-decision time';'(Load 1)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
% add drift
ax{2} = subplot(1,3,2); hold on;
    y = STSWD_summary.HDDM_vt.nondecisionMRI_linear(idx_AttFactor,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    %title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(3,1),2)),',',num2str(round(correlationCI(3,2),2)),']'])
    ylabel({'Non-decision time';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
% add drift change
ax{3} = subplot(1,3,3); hold on;
    y = STSWD_summary.HDDM_vt.thresholdMRI(idx_AttFactor,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
   % title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(6,1),2)),',',num2str(round(correlationCI(6,2),2)),']']) 
    ylabel({'Threshold';'(Load 1)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])

set(h,'Color','w')
for indAx = 1:6; ax{indAx}.Color = [.2 .2 .2]; end

h = figure('units','normalized','position',[.1 .1 .3 .25/2]);
% add AMF
x = individualBrainScores_slope(idx_BS);
%x = squeeze(nanmean(individualBrainScores(:,idx_BS),1));
ax{1} = subplot(1,3,1); hold on;
    y = squeeze(nanmean(STSWD_summary.SSVEPmag.data_norm(idx_AttFactor,1:4),2));
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    %title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(2,1),2)),',',num2str(round(correlationCI(2,2),2)),']'])
    ylabel({'Non-decision time';'(Load 1)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
% add drift
ax{2} = subplot(1,3,2); hold on;
    y = STSWD_summary.SSVEPmag.data_norm_linear(idx_AttFactor,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    %title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(3,1),2)),',',num2str(round(correlationCI(3,2),2)),']'])
    ylabel({'Non-decision time';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])

set(h,'Color','w')
for indAx = 1:6; ax{indAx}.Color = [.2 .2 .2]; end

h = figure('units','normalized','position',[.1 .1 .3 .25/2]);
% add AMF
x = squeeze(nanmean(individualBrainScores(idx_BS,:),2));
ax{1} = subplot(1,3,1); hold on;
    y = squeeze(nanmean(STSWD_summary.SSVEPmag.data_norm(idx_AttFactor,1:4),2));
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    %title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(2,1),2)),',',num2str(round(correlationCI(2,2),2)),']'])
    ylabel({'Non-decision time';'(Load 1)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
% add drift
ax{2} = subplot(1,3,2); hold on;
    y = STSWD_summary.SSVEPmag.data_norm_linear(idx_AttFactor,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    %title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(3,1),2)),',',num2str(round(correlationCI(3,2),2)),']'])
    ylabel({'Non-decision time';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])

set(h,'Color','w')
for indAx = 1:6; ax{indAx}.Color = [.2 .2 .2]; end


%% correlate with thalamic score
% 
% load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/B_data/MeanBOLD_ER_PLS_v4_stBL/behavPLS_ER_fullModel_win_STSWD_YA_N42_3mm_1000P10_fMRIresult_bs.mat'], 'BS')
% 
% idx_AttFactor = find(ismember(BS.IDs, IDs));
% idx_BS = find(ismember(IDs,BS.IDs));
% 
% 
% h = figure('units','normalized','position',[.1 .1 .3 .2]);
% x = individualBrainScores_slope(idx_BS);
% % add AMF
% ax{1} = subplot(1,2,1); cla; hold on;
%     x = individualBrainScores_slope(idx_BS);
%     y = BS.behav(idx_AttFactor,1);
%     scatter(x, y, 'filled', 'MarkerFaceColor', [.5 1 1]);
%     [r, p] = corrcoef(x,y)
%     ylabel({'behavPLS: behavioral score';'(linear modulation)'})
%     xlabel({'meanPLS: BOLD entropy';'(linear modulation)'})
% ax{2} = subplot(1,2,2); cla; hold on;
%     x = individualBrainScores_slope(idx_BS);
%     y = BS.data(idx_AttFactor,1);
%     scatter(x, y, 'filled', 'MarkerFaceColor', [1 .6 .6]);
%     [r, p] = corrcoef(x,y)
%     ylabel({'behavPLS: brainscore';'(linear modulation)'})
%     xlabel({'meanPLS: BOLD entropy';'(linear modulation)'})
%     
% % make background black, axes labels white
% set(h,'Color','k')
% for indAx = 1:2; ax{indAx}.Color = [0 0 0]; ax{indAx}.XColor = [1 1 1]; ...
%         ax{indAx}.YColor = [1 1 1]; end
% 
% set(findall(gcf,'-property','FontSize'),'FontSize',16)

%% fit multilinear regression

% individualBrainScores = individualBrainScores';
% 
% h = figure('units','normalized','position',[.1 .1 .2 .3]);
%     cla; hold on;
%     for indID = 1:42
%         a = individualBrainScores(idx_BS(indID),1:4)-nanmean(individualBrainScores(idx_BS(indID),1:4))+nanmean(nanmean(individualBrainScores(idx_BS(indID),1:4),1),2);
%         b = STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4),2),1);
%         %b = STSWD_summary.SE.data(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.SE.data(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.SE.data(idx_AttFactor(indID),1:4),2),1);
%         %b = STSWD_summary.OneFslope.data(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.OneFslope.data(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.OneFslope.data(idx_AttFactor(indID),1:4),2),1);
%         %b = STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4),2),1);
%         %b = STSWD_summary.SSVEPmag.data_norm(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.SSVEPmag.data_norm(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.SSVEPmag.data_norm(idx_AttFactor(indID),1:4),2),1);
%         ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
%         scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
%     end
%     xlabel('Brainscore'); ylabel('drift');
%     title({'Multivariate spectral changes inter-individually';'relate to drift effect'})
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
% 
% % multiple linear mixed effects regression:
% % brainscore = b0 + b1(pupil) + b2(categorical subject) + e
% 
% % centered_behav = STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(:),1:4)-nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(:),1:4))+nanmean(nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(:),1:4),2),1);
% % centered_brain = individualBrainScores(idx_BS(:),1:4)-nanmean(individualBrainScores(idx_BS(:),1:4))+nanmean(nanmean(individualBrainScores(idx_BS(:),1:4),1),2);
% 
% % assemble table
% Subject = repmat(IDs,1,4); Subject = reshape(Subject', 42*4,1);
% Condition = repmat([1:4]',42,1);
% %Behav = reshape(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor,1:4)', [],1);
% %Behav = reshape(STSWD_summary.SE.data(idx_AttFactor,1:4)', [],1);
% %Behav = reshape(STSWD_summary.OneFslope.data(idx_AttFactor,1:4)', [],1);
% %Behav = reshape(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor,1:4)', [],1);
% %Behav = reshape(STSWD_summary.SSVEPmag.data_norm(idx_AttFactor,1:4)', [],1);
% BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
% 
% tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
% lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
% lme

%% Relation of LV2 to NDT

individualBrainScores = individualBrainScores';

h = figure('units','normalized','position',[.1 .1 .35 .25]);
x = squeeze(nanmean(individualBrainScores(idx_BS,1:4),2));
ax{1} = subplot(1,2,1); hold on;
    y = STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y)
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    ylabel({'Non-decision time';'(Load 1)'}); xlabel('Brainscore (avg. L1-4)')
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])

    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
ax{2} = subplot(1,2,2); hold on;
    cla; hold on;
    for indID = 1:42
        a = individualBrainScores(idx_BS(indID),1:4)-nanmean(individualBrainScores(idx_BS(indID),1:4))+nanmean(nanmean(individualBrainScores(idx_BS(indID),1:4),1),2);
        b = STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4),2),1);
        ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
        scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    end
    xlabel('Brainscore'); ylabel('NDT');
    title({'Individual brainscore changes';'relate to NDT increases'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)


% alternatively plot drift rate
    
h = figure('units','normalized','position',[.1 .1 .35 .25]);
x = squeeze(nanmean(individualBrainScores(idx_BS,1:4),2));
ax{1} = subplot(1,2,1); hold on;
    y = STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y)
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    ylabel({'Drift';'(Load 1)'}); xlabel('Brainscore (avg. L1-4)')
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])

    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
ax{2} = subplot(1,2,2); hold on;
    cla; hold on;
    for indID = 1:42
        a = individualBrainScores(idx_BS(indID),1:4)-nanmean(individualBrainScores(idx_BS(indID),1:4))+nanmean(nanmean(individualBrainScores(idx_BS(indID),1:4),1),2);
        b = STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4),2),1);
        ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
        scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    end
    xlabel('Brainscore'); ylabel('Drift');
    title({'Individual brainscore changes';'relate to NDT increases'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)


    
% assemble table
Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
lme

lme.Coefficients

% assemble table
Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
lme

% assemble table
Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.pupil2.stimdiff(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
lme

Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.SE.data(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
lme

Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.OneFslope.data(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
lme

Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.EEG_LV1.data(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
lme

Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.pupil2.stimdiff(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme

Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(STSWD_summary.pupil2.stimdiff(idx_AttFactor,1:4)', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme


Subject = repmat(IDs,1,4); Subject = reshape(Subject', [],1);
Condition = repmat([1:4]',42,1);
Behav = reshape(squeeze(STSWD_summary.PCAdim.dim(idx_AttFactor,17,1:4))', [],1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4)', [],1);
tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
lme = fitlme(tbl,'Behav~BrainScore+(1|Subject)', 'CovariancePattern', 'CompSymm');
%lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme


h = figure('units','normalized','position',[.1 .1 .35 .25]);
x = squeeze(nanmean(individualBrainScores(idx_BS,1:4),2));
ax{1} = subplot(1,2,1); hold on;
    y = squeeze(nanmean(STSWD_summary.EEG_prestim.data(idx_AttFactor,1:4),2));
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y)
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    ylabel({'Drift';'(Load 1)'}); xlabel('Brainscore (avg. L1-4)')
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])

    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
ax{2} = subplot(1,2,2); hold on;
    cla; hold on;
    for indID = 1:42
        a = individualBrainScores(idx_BS(indID),1:4)-nanmean(individualBrainScores(idx_BS(indID),1:4))+nanmean(nanmean(individualBrainScores(idx_BS(indID),1:4),1),2);
        b = STSWD_summary.EEG_prestim.data(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.EEG_prestim.data(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.EEG_prestim.data(idx_AttFactor(indID),1:4),2),1);
        ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
        scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    end
    xlabel('Brainscore'); ylabel('Drift');
    title({'Individual brainscore changes';'relate to NDT increases'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
