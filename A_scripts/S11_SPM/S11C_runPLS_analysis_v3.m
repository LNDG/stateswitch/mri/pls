clear all; clc; restoredefaultpath

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.plsroot      = [pn.root, 'analyses/B4_PLS_preproc2/'];
pn.plstoolbox   = [pn.root, 'analyses/B6_PLS_eventRelated/T_tools/pls/']; addpath(genpath(pn.plstoolbox));

cd([pn.plsroot, 'B_data/SPM_STSWD_v3/']);

batch_plsgui('meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIanalysis.txt')

% behavioral PLS
batch_plsgui('behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIanalysis.txt')

cd([pn.plsroot, 'B_data/SPM_STSWD_v32/']);

% behavioral PLS
batch_plsgui('behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui
