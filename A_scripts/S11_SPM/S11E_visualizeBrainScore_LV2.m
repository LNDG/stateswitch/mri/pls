%% Plot brainscores +- CI outside PLS

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data//mri/task/analyses/D_PLS_Dim/';
pn.functions    = [pn.root, 'T_tools/']; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]);

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools/')
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools/RainCloudPlots'))

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/';
load([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult.mat'])

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

% N = 42 YA, 1125, 1214 excluded: no EEG data; 1151,1252,1228,1247 excluded before, not here
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:numel(groupsizes)
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,2); % vsc: designscore
        uData{indGroup}(indCond,:) = result.usc(targetEntries,2); % usc: brainscore
    end
end

individualBrainScores = uData{1};
individualBehavScores = condData{1};


indLV = 2;
meanCent = result.boot_result.orig_usc(:,indLV)';
ulusc_meanCent = result.boot_result.ulusc(:,indLV)';
llusc_meanCent = result.boot_result.llusc(:,indLV)';

meanY = [nanmean(uData{1},2)]';

ulusc_NonMeanCent = ulusc_meanCent+(meanY-meanCent);
llusc_NonMeanCent = llusc_meanCent+(meanY-meanCent);

ulusc = ulusc_meanCent-meanCent;
llusc = llusc_meanCent-meanCent;

% figure; 
% subplot(1,2,1); hold on; bar(meanCent); plot(ulusc_meanCent); plot(llusc_meanCent)
% subplot(1,2,2); hold on; bar(meanY); plot(ulusc_NonMeanCent); plot(llusc_NonMeanCent)

errorY{1} = [llusc(1:4); ulusc(1:4)];
errorY{2} = [llusc(5:end); ulusc(5:end)];

%% plot RainCloudPlot

h = figure('units','normalized','position',[.1 .1 .15 .2]);
set(gcf,'renderer','Painters')
curData = uData{indGroup}';

 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = 2.*[.3 .1 .1];

    box off
    cla;
        h_rc = rm_raincloud(data_ws, cl,1);
        % add stats
        condPairs = [1,2; 2,3; 3,4];
        condPairsLevel = [19000, 21000, 18000];
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Targets'); xlabel({'Brainscore (a.u.)'})
    %title('1/f slope modulation'); 
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim([-13000 22000]); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/C_figures/S11/';
figureName = 'S11_meanPLS_stim_LV2';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% save brainscores

BS.data = individualBrainScores;
BS.behav = individualBehavScores;
BS.IDs = IDs;
save([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult_lv2_bs.mat'], 'BS')
