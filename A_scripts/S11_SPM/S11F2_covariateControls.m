
%% load brainscores

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/';
load([pn.data, 'behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult_bs.mat'], 'BS')

% BS.data = individualBrainScores;
% BS.behav = individualBehavScores;
% BS.IDs = IDs;

%% assess covariation with age

demo = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.mat', 'T', 'C', 'Age');

demoIDs = cellstr(num2str(demo(:).T.x_id));
idx_demo = ismember(demoIDs, BS.IDs);
%[demoIDs(idx_demo),BS.IDs]

% regression (correlation)

figure; scatter(BS.data, demo.Age(idx_demo))
[r,p] = corrcoef(BS.data, demo.Age(idx_demo))

figure; scatter(BS.behav, demo.Age(idx_demo))
[r,p] = corrcoef(BS.behav, demo.Age(idx_demo))

%% assess covariation with gender
% note that 1 codes female, 2 codes male

gender = demo.T.sex;

figure; scatter(BS.data, gender(idx_demo))
[r,p]=corrcoef(BS.data, gender(idx_demo))

figure; scatter(BS.behav, gender(idx_demo))
[r,p]= corrcoef(BS.behav, gender(idx_demo))

%% difficulty

difficulty = cell2mat(demo.C(:,73));

figure; scatter(BS.data, difficulty(idx_demo))
[r,p]=corrcoef(BS.data, difficulty(idx_demo))

figure; scatter(BS.behav, difficulty(idx_demo))
[r,p]= corrcoef(BS.behav, difficulty(idx_demo))

%% assess covariation with BOLD motion (framewise displacement)

for indID = 1:numel(BS.IDs)
    curFile = ['/Users/kosciessa/Desktop/mntTardisLNDG/kosciessa/StateSwitch/WIP/G_GLM/B_data/M_MotionParameters/', ...
        BS.IDs{indID}, '/',BS.IDs{indID},'_avgsess_FD_abs.txt'];
    FD(indID) = load(curFile);
end

figure; scatter(BS.data, FD)
[r,p]=corrcoef(BS.data, FD)

figure; scatter(BS.behav, FD)
[r,p]=corrcoef(BS.behav, FD)
