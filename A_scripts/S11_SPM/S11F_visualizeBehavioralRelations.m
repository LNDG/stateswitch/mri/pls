%% Plot brainscores +- CI outside PLS

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/';
pn.functions    = [pn.root, 'T_tools/']; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]);

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/';
load([pn.data, 'behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult.mat'])

% N = 42 YA, 1125, 1214 excluded: no EEG data; 1151,1252,1228,1247 excluded before, not here
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'linear1234'};

condData = []; uData = []; u2Data = [];
for indGroup = 1:1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

correlationCI = [result.boot_result.ulcorr(:,1),result.boot_result.llcorr(:,1)];

individualBrainScores = uData{1}';
individualBehavScores = condData{1}';

%% important: invert scores to have positives in thalamus

individualBrainScores = -1.*individualBrainScores;
correlationCI = -1.*correlationCI;

% save brainscores

% BS.data = individualBrainScores;
% BS.behav = individualBehavScores;
% BS.IDs = IDs;
% save([pn.data, 'behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult_bs.mat'], 'BS')

%% reproduce correlation plots

h = figure('units','normalized','position',[.1 .1 .3 .25]);
% add AMF
x = individualBrainScores;
ax{1} = subplot(2,3,1); hold on;
	y = behavdata_lst{1,1}(:,1);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
%    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3)), ' CI = [',num2str(round(correlationCI(1,1),2)),',',num2str(round(correlationCI(1,2),2)),']'])
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(1,1),2)),',',num2str(round(correlationCI(1,2),2)),']'])
    ylabel({'spectral BS';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
% add drift
ax{2} = subplot(2,3,2); hold on;
    y = behavdata_lst{1,1}(:,4);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(4,1),2)),',',num2str(round(correlationCI(4,2),2)),']'])
    ylabel({'Drift Load 1'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
% add drift change
ax{3} = subplot(2,3,3); hold on;
    y = behavdata_lst{1,1}(:,5);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(5,1),2)),',',num2str(round(correlationCI(5,2),2)),']']) 
    ylabel({'Drift';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
% add 1/f
ax{4} = subplot(2,3,4); hold on;
    y = behavdata_lst{1,1}(:,7);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(7,1),2)),',',num2str(round(correlationCI(7,2),2)),']']) 
    ylabel({'1/f slope';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
% add entropy
ax{5} = subplot(2,3,5); hold on;
    y = behavdata_lst{1,1}(:,8);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(8,1),2)),',',num2str(round(correlationCI(8,2),2)),']'])    
    ylabel({'entropy';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
% add pupildiff
ax{6} = subplot(2,3,6); hold on;
    y = behavdata_lst{1,1}(:,9);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title({['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(9,1),2)),',',num2str(round(correlationCI(9,2),2)),']']})        
    ylabel({'pupil';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

% % make background black, axes labels white
% set(h,'Color','k')
% for indAx = 1:6; ax{indAx}.Color = [0 0 0]; ax{indAx}.XColor = [1 1 1]; ...
%         ax{indAx}.YColor = [1 1 1]; end

set(h,'Color','w')
for indAx = 1:6; ax{indAx}.Color = [.2 .2 .2]; end

set(findall(gcf,'-property','FontSize'),'FontSize',14)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/C_figures/S11/';
figureName = 'S11_behavPLS_corrs';
h.InvertHardcopy = 'off';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot unrelated signals

h = figure('units','normalized','position',[.1 .1 .3 .25/2]);
% add AMF
x = individualBrainScores;
ax{1} = subplot(1,3,1); hold on;
	y = behavdata_lst{1,1}(:,2);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.5 1 1]); l1 = lsline(); set(l1, 'Color',[.5 1 1], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
%    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3)), ' CI = [',num2str(round(correlationCI(1,1),2)),',',num2str(round(correlationCI(1,2),2)),']'])
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(2,1),2)),',',num2str(round(correlationCI(2,2),2)),']'])
    ylabel({'Non-decision time';'(Load 1)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
% add drift
ax{2} = subplot(1,3,2); hold on;
    y = behavdata_lst{1,1}(:,3);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(3,1),2)),',',num2str(round(correlationCI(3,2),2)),']'])
    ylabel({'Non-decision time';'(linear mod.)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
% add drift change
ax{3} = subplot(1,3,3); hold on;
    y = behavdata_lst{1,1}(:,6);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' CI = [',num2str(round(correlationCI(6,1),2)),',',num2str(round(correlationCI(6,2),2)),']']) 
    ylabel({'Threshold';'(Load 1)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])

set(h,'Color','w')
for indAx = 1:6; ax{indAx}.Color = [.2 .2 .2]; end

set(findall(gcf,'-property','FontSize'),'FontSize',14)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/C_figures/S11/';
figureName = 'S11_behavPLS_corrs_noInterest';
h.InvertHardcopy = 'off';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');


%% fit multilinear regression

individualBrainScores = individualBrainScores';

h = figure('units','normalized','position',[.1 .1 .2 .3]);
    cla; hold on;
    for indID = 1:42
        a = individualBrainScores(idx_BS(indID),1:4)-nanmean(individualBrainScores(idx_BS(indID),1:4))+nanmean(nanmean(individualBrainScores(idx_BS(indID),1:4),1),2);
        %b = STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor(indID),1:4),2),1);
        %b = STSWD_summary.SE.data(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.SE.data(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.SE.data(idx_AttFactor(indID),1:4),2),1);
        %b = STSWD_summary.OneFslope.data(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.OneFslope.data(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.OneFslope.data(idx_AttFactor(indID),1:4),2),1);
        b = STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4)-nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4))+nanmean(nanmean(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor(indID),1:4),2),1);
        ls_1 = polyval(polyfit(a, b,1),a); ls_1 = plot(a, ls_1, 'Color', [0 0 0], 'LineWidth', 3);
        scatter(a, b, 70, 'filled','MarkerFaceColor', [0 0 0]);
    end
    xlabel('Brainscore'); ylabel('drift');
    title({'Multivariate spectral changes inter-individually';'relate to drift effect'})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)


% multiple linear mixed effects regression:
% brainscore = b0 + b1(pupil) + b2(categorical subject) + e

% assemble table
Subject = repmat(IDs,1,4); Subject = reshape(Subject', 42*4,1);
Condition = repmat([1:4]',42,1);
%Behav = reshape(STSWD_summary.HDDM_vt.driftMRI(idx_AttFactor,1:4)', 42*4,1);
%Behav = reshape(STSWD_summary.SE.data(idx_AttFactor,1:4)', 42*4,1);
%Behav = reshape(STSWD_summary.OneFslope.data(idx_AttFactor,1:4)', 42*4,1);
Behav = reshape(STSWD_summary.HDDM_vt.nondecisionMRI(idx_AttFactor,1:4)', 42*4,1);
BrainScore = reshape(individualBrainScores(idx_BS,1:4), 42*4,1);

tbl = table(double(Condition),double(Behav),double(BrainScore),Subject,'VariableNames',{'Condition','Behav','BrainScore', 'Subject'});
lme = fitlme(tbl,'Behav~BrainScore+(1+BrainScore|Subject)', 'CovariancePattern', 'CompSymm');
lme

