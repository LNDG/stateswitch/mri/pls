% extract probability locations using the Anatomy toolbox

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/';

%% task PLS: LV1

% export peak locations
load([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv1_BfMRIresult_BfMRIcluster.mat'])
locs = cluster_info.data{1}.peak_loc;
fid = fopen([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv1_BfMRIresult_BfMRIcluster_forSPM.txt'],'wt');
for ii = 1:size(locs,1)
    fprintf(fid,'%g\t',locs(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

% export BSR and voxel amount
BSRval = cluster_info.data{1}.peak_values';
BSRval = round(BSRval*100)/100;
BSRval = [BSRval, cluster_info.data{1}.size'];
fid = fopen([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv1_BfMRIresult_BfMRIcluster_BSR.txt'],'wt');
for ii = 1:size(BSRval,1)
    fprintf(fid,'%g\t',BSRval(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

%% task PLS: LV2

load([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv2_BfMRIresult_BfMRIcluster.mat'])
locs = cluster_info.data{1}.peak_loc;
% save as .txt
fid = fopen([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv2_BfMRIresult_BfMRIcluster_forSPM.txt'],'wt');
for ii = 1:size(locs,1)
    fprintf(fid,'%g\t',locs(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

% export BSR and voxel amount
BSRval = cluster_info.data{1}.peak_values';
BSRval = round(BSRval*100)/100;
BSRval = [BSRval, cluster_info.data{1}.size'];
fid = fopen([pn.data, 'meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv2_BfMRIresult_BfMRIcluster_BSR.txt'],'wt');
for ii = 1:size(BSRval,1)
    fprintf(fid,'%g\t',BSRval(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

%% behavioral PLS: LV1

load([pn.data, 'behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv1_BfMRIresult_BfMRIcluster.mat'])
locs = cluster_info.data{1}.peak_loc;
% save as .txt
fid = fopen([pn.data, 'behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv1_BfMRIresult_BfMRIcluster_forSPM.txt'],'wt');
for ii = 1:size(locs,1)
    fprintf(fid,'%g\t',locs(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

% export BSR and voxel amount
BSRval = cluster_info.data{1}.peak_values';
BSRval = round(BSRval*100)/100;
BSRval = [BSRval, cluster_info.data{1}.size'];
fid = fopen([pn.data, 'behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_lv1_BfMRIresult_BfMRIcluster_BSR.txt'],'wt');
for ii = 1:size(BSRval,1)
    fprintf(fid,'%g\t',BSRval(ii,:));
    fprintf(fid,'\n');
end
fclose(fid)

%% external: batch process the peak locations

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B6_PLS_eventRelated/T_tools/spm12')

spm