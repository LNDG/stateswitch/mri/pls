% plot histogram of BSR voxels in Horn subregions
clear all

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/I_thalamicNuclei/B_data/';
pn.dataBSR = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/';

addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/T_tools/NIFTI_toolbox'))
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/T_tools/preprocessing_tools'))

%% invert 1st behavioral PLS component for visualization

BSRimg_path = [pn.dataBSR, 'SPM_STSWD_v3/behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv1.img'];
BSRimg_pathOut = [pn.dataBSR, 'SPM_STSWD_v3/behavPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv1_inverted.img'];

tempNii = load_nii(BSRimg_path);
tempNii.img = tempNii.img.*-1; % invert 3D matrix
save_nii(tempNii,BSRimg_pathOut)
        
%% invert 2nd task PLS component for visualization

BSRimg_path = [pn.dataBSR, 'SPM_STSWD_v3/meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv2.img'];
BSRimg_pathOut = [pn.dataBSR, 'SPM_STSWD_v3/meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIbsr_lv2_inverted.img'];

tempNii = load_nii(BSRimg_path);
tempNii.img = tempNii.img.*-1; % invert 3D matrix
save_nii(tempNii,BSRimg_pathOut)
