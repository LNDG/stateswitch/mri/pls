clear all; clc; restoredefaultpath

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.plsroot      = [pn.root, 'analyses/B4_PLS_preproc2/'];
%pn.plstoolbox   = [pn.root, 'analyses/D2_PLS_VarTbx/T_tools/PLS_LNDG2018/Pls/']; addpath(genpath(pn.plstoolbox));
pn.plstoolbox   = [pn.root, 'analyses/B6_PLS_eventRelated/T_tools/pls/']; addpath(genpath(pn.plstoolbox));

cd([pn.plsroot, 'B_data/Mean_STSWD/']);

batch_plsgui('meancentPLS_STSWD_Mean_N44_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_Mean_N44_subgroups_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_MeanAcrossCondFix_N44_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_MeanAcrossCondFix_1234_N44_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_MeanAcrossCondITI_N44_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_MeanAcrossCondITI_1234_N44_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('meancentPLS_STSWD_MeanAcrossCondFix_subgroups_1234_N44_3mm_1000P1000B_BfMRIanalysis.txt')

% behavioral PLS

batch_plsgui('behavPLS_STSWD_MeanAcrossCondFix_dim_AMF_N44_3mm_1000P1000B_BfMRIanalysis.txt') % dimensionality (GM, STIM) + EEG AMF; no sign. LV
batch_plsgui('behavPLS_STSWD_Mean_dim_AMF_N44_3mm_1000P1000B_BfMRIanalysis.txt') % dimensionality (GM, STIM) + EEG AMF; no sign. LV

% full model with PLS-estimated Mean BOLD

batch_plsgui('behavPLS_STSWD_Mean_fullModel_N42_3mm_1000P1000B_BfMRIanalysis.txt') % dimensionality (GM, STIM) + EEG AMF; no sign. LV

plsgui