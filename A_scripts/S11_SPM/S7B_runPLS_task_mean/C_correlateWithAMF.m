%% Plot brainscores +- CI outside PLS

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/';
pn.functions    = [pn.root, 'T_tools/']; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]);

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/Mean_STSWD/'; 
load([pn.data, 'meancentPLS_STSWD_Mean_N44_3mm_1000P1000B_BfMRIresult.mat']);

% IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
%     '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
%     '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
%     '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% get IDs directly from PLS result
IDs = cellfun(@(x) x(6:end), subj_name, 'un', 0)';

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:1
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

individualBrainScores = uData{1}';
individualBehavScores = condData{1}';

%% save brainscores

BS.data = individualBrainScores;
BS.behav = individualBehavScores;
BS.IDs = IDs;
save([pn.data, 'meancentPLS_STSWD_Mean_N44_3mm_1000P1000B_BfMRIresult_lv1_bs.mat'], 'BS')

%% WIP behavioral relations

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

jointsubs = intersect(EEGAttentionFactor.IDs,IDs);
attFctrIdx = find(ismember(EEGAttentionFactor.IDs,jointsubs));
PLSIdx = find(ismember(IDs,jointsubs));

% check subject equivalence here
% IDs(PLSIdx(20))
% EEGAttentionFactor.IDs(attFctrIdx(20))

figure; 
scatter(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,2:4),2), 'filled')
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,2:4),2))

scatter(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,2:4),2)-individualBrainScores(PLSIdx,1), 'filled')
[r, p] = corrcoef(EEGAttentionFactor.PCAalphaGamma(attFctrIdx), nanmean(individualBrainScores(PLSIdx,2:4),2)-individualBrainScores(PLSIdx,1))

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

jointsubs = intersect(STSWD_summary.IDs,IDs);
sumIdx = find(ismember(STSWD_summary.IDs,jointsubs));
PLSIdx = find(ismember(IDs,jointsubs));

figure; 
scatter(squeeze(nanmean(STSWD_summary.HDDM.driftMRI(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,2:4),2))-...
    squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'filled')
[r, p] = corrcoef(squeeze(nanmean(STSWD_summary.HDDM.driftMRI(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,2:4),2))-...
    squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'rows', 'Complete')

figure;
STSWD_summary.behav.EEGAcc(21) = NaN;
scatter(squeeze(nanmean(STSWD_summary.behav.EEGAcc(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'filled')
[r, p] = corrcoef(squeeze(nanmean(STSWD_summary.behav.EEGAcc(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'rows', 'Complete')

figure;
STSWD_summary.behav.EEGAcc(21) = NaN;
scatter(squeeze(nanmean(STSWD_summary.behav.MRIAcc(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'filled')
[r, p] = corrcoef(squeeze(nanmean(STSWD_summary.behav.MRIAcc(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'rows', 'Complete')


figure;
STSWD_summary.behav.MRIAcc(STSWD_summary.behav.MRIAcc<.7) = NaN;
scatter(squeeze(nanmean(STSWD_summary.behav.MRIAcc(sumIdx,2:4),2)), ...
    squeeze(nanmean(individualBrainScores(PLSIdx,2:4),2))-...
    squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'filled')
[r, p] = corrcoef(squeeze(nanmean(STSWD_summary.behav.MRIAcc(sumIdx,2:4),2)), ...
    squeeze(nanmean(individualBrainScores(PLSIdx,2:4),2))-...
    squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'rows', 'Complete')


figure;
STSWD_summary.behav.MRIAcc(STSWD_summary.behav.MRIAcc<.7) = NaN;
scatter(squeeze(nanmean(STSWD_summary.behav.MRIAcc(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'filled')
[r, p] = corrcoef(squeeze(nanmean(STSWD_summary.behav.MRIAcc(sumIdx,1),2)), squeeze(nanmean(individualBrainScores(PLSIdx,1),2)), 'rows', 'Complete')


%% plot uncentered data

groups = {'Young adults'};

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel = [2, 3, 4, 5, 6, 7];

indLV = 1;
meanCent = result.boot_result.orig_usc(:,indLV)';
ulusc_meanCent = result.boot_result.ulusc(:,indLV)';
llusc_meanCent = result.boot_result.llusc(:,indLV)';

meanY = [nanmean(uData{1},2)]';

ulusc_NonMeanCent = ulusc_meanCent+(meanY-meanCent);
llusc_NonMeanCent = llusc_meanCent+(meanY-meanCent);

ulusc = ulusc_meanCent-meanCent;
llusc = llusc_meanCent-meanCent;

% figure; 
% subplot(1,2,1); hold on; bar(meanCent); plot(ulusc_meanCent); plot(llusc_meanCent)
% subplot(1,2,2); hold on; bar(meanY); plot(ulusc_NonMeanCent); plot(llusc_NonMeanCent)

errorY{1} = [llusc(1:4); ulusc(1:4)];
errorY{2} = [llusc(5:end); ulusc(5:end)];

h = figure('units','normalized','position',[.1 .1 .7 .4]);
for indGroup = 1%:2
    subplot(1,2,indGroup);
    meanY = nanmean(uData{indGroup},2);    
    %errorY = nanstd(uData{indGroup},[],2)/sqrt(size(uData{indGroup},2));
    [h1, hError] = barwitherr(errorY{indGroup}', meanY);
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(uData{indGroup}(condPairs(indPair,1),:), uData{indGroup}(condPairs(indPair,2), :)); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    set(h1(1),'FaceColor',colorm(1,:));
    set(h1(1),'LineWidth',2);
    set(hError(1),'LineWidth',3);
    box(gca,'off')
    set(gca, 'XTick', [1,2,3,4]);
    set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
    xlabel('Target load')
    %xlabel('Target condition'); 
    ylabel({'Brainscore (a.u.)'})
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    ylim([-20 10]); xlim([.25 4.75]);
    title(groups{indGroup})
end

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B3_PLS_mean_v3/C_figures/';
figureName = 'S1_meancentPLS_Mean_v2_YAOA_N97_nonMeanCent';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

