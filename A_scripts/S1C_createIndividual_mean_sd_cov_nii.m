function S01_createIndividual_mean_sd_cov_nii()

% Create individual Mean, SD and COV images.

% 171220 | adapted by JQK; use only non-zero-power GM voxels
% 180202 | adapted for repeats
% 180302 | non-zero voxels lost occipital cortex, use non-NaN voxels

% If data is missing (e.g. a run), command window issues a warning, but
% continues with the next available data.

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B4_PLS_preproc2/T_tools/'];  addpath(genpath(pn.tools));
pn.data     = [pn.root, 'analyses/B4_PLS_preproc2/B_data/'];
MATPATH     = ([pn.data,'SD_STSWD_task_v2/']);
NIIPATH     = ([pn.data,'BOLDin/']);
COORDPATH   = [pn.data, 'VoxelOverlap/'];

% N = 44 YA + 53 OA; '1126' currently removed (no T2w)
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}, '.']);

%% load templates with information about run structure

    numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc

    % load subject's sessiondata file
    a = load([MATPATH, 'task_', IDs{indID}, '_BfMRIsessiondata']);
    conditions=a.session_info.condition(1:numConds_raw);
    % intialize cond specific scan count for populating cond_data
    clear count cond_data block_scan;
    for indCond = 1:numel(conditions)
        count{indCond} = 0;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % within each indBlock express each scan as deviation from indBlock's
    % temporal mean.Concatenate all these deviation values into one
    % long condition specific set of scans that were normalized for
    % indBlock-to-indBlock differences in signal strength. In the end calculate
    % stdev across all normalized indCond scans
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % for each condition identify its scans within  runs
    % and prepare where to put indCond specific normalized data
    for indCond = 1:numel(conditions)
        tot_num_scans = 0;
        for run = 1:a.session_info.num_runs
            if a.session_info.run(run).num_scans==0
                disp(['No data for ' IDs{indID} ' in ', num2str(run)]);
                continue;
            end
            onsets = a.session_info.run(run).blk_onsets{indCond}+1; % +1 is because we need matlab indexing convention
            lengths = a.session_info.run(run).blk_length{indCond};
            for indBlock = 1:numel(onsets)
                block_scans{indCond}{run}{indBlock} = onsets(indBlock)-1+[1:lengths(indBlock)];
                this_length = lengths(indBlock);
                if max(block_scans{indCond}{run}{indBlock}>a.session_info.run(run).num_scans)
                    disp(['Problem: ' IDs{indID} ' something wrong in ', num2str(run)]);
                    block_scans{indCond}{run}{indBlock} = intersect(block_scans{indCond}{run}{indBlock},[1:a.session_info.run(run).num_scans]);
                    this_length = numel(block_scans{indCond}{run}{indBlock});
                end
                tot_num_scans = tot_num_scans + this_length;
            end
        end
    end


    %% Load NIfTI file

    for indRun = 1:a.session_info.num_runs

        %% Load NIfTI
        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        fname = [a.session_info.run(indRun).data_path '/' a.session_info.run(indRun).data_files{:}];
        
        if ~exist(fname) || isempty(a.session_info.run(indRun).data_files)
            warning(['File not available: ', IDs{indID}, ' Run ', num2str(indRun)]);
            continue;
        end
        
        % If using preprocessing tools
        [ img ] = double(S_load_nii_2d( fname ));

        % use all coordinates here!
        
        %% Now, proceed with creating SD datamat...

        for indCond = 1:numel(conditions)
            for indBlock = 1:numel(block_scans{indCond}{run})
                block_data = img(:,block_scans{indCond}{run}{indBlock});% (vox time)
                % normalize block_data to global block mean = 100.
                block_data = 100*block_data/mean(mean(block_data));
                % temporal mean of this indBlock
                block_mean = mean(block_data,2); % (vox) - mean of this should be 100
                % express scans in this indBlock as deviations from block_mean
                % and append to cond_data
                for t = 1:size(block_data,2)
                    count{indCond} = count{indCond} + 1;
                    cond_data{indCond}(:,count{indCond}) = block_data(:,t) - block_mean(:); % JQK: use all voxels.
                    cond_data_withMean{indCond}(:,count{indCond}) = block_data(:,t); % JQK: use all voxels.
                end
            end
        end
    end

    %% Calculate the measures of interest

    % now calculate stdev across all indCond scans.
    dataMat_SD = []; dataMat_mean = []; dataMat_cov = [];
    for indCond = 1:numel(conditions)
        %% SD BOLD
        dataMat_SD(indCond,:) = squeeze(std(cond_data{indCond},0,2))';        
        % create individual Nifty file
        tempNii = load_nii([COORDPATH, '2009c_brain_3mm_str.nii']);
        tempNii.img = reshape(dataMat_SD(indCond,:), 60,72,60); % reshape to 3D matrix
        save_nii(tempNii,[pn.data, 'X1_IndividualSD/SD_',IDs{indID}, '_',conditions{indCond},'.nii'])
        
        %% Mean BOLD
        dataMat_mean(indCond,:) = squeeze(mean(cond_data_withMean{indCond},2))';        
        % create individual Nifty file
        tempNii = load_nii([COORDPATH, '2009c_brain_3mm_str.nii']);
        tempNii.img = reshape(dataMat_mean(indCond,:), 60,72,60); % reshape to 3D matrix
        save_nii(tempNii,[pn.data, 'X2_IndividualMean/Mean_',IDs{indID}, '_',conditions{indCond},'.nii'])
        
        %% COV BOLD
        dataMat_cov(indCond,:) = (squeeze(std(cond_data{indCond},0,2))'./squeeze(mean(cond_data_withMean{indCond},2))').*100;        
        % create individual Nifty file
        tempNii = load_nii([COORDPATH, '2009c_brain_3mm_str.nii']);
        tempNii.img = reshape(dataMat_cov(indCond,:), 60,72,60); % reshape to 3D matrix
        save_nii(tempNii,[pn.data, 'X3_IndividualCOV/COV_',IDs{indID}, '_',conditions{indCond},'.nii'])
    end

    % all values get saved in the datamat below; nothing should need to be
    % saved to session files at this point, so leave those as is.
    clear img;
  
    disp ([IDs{indID} ' done!'])
    
end