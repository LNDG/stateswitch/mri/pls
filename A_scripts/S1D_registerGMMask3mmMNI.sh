#!/bin/bash

regions="mni_icbm152_gm_tal_nlin_sym_09c"

BASE='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/A_standards/mni_icbm152_nlin_sym_09c/'
REF='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/A_standards/2009c_brain_3mm_str.nii'

for reg in $regions; do
flirt -in ${BASE}${reg}.nii -ref ${REF} -applyxfm -usesqform -out ${BASE}${reg}_thr_mask.nii.gz
fslmaths ${BASE}${reg}_thr_mask.nii.gz -thr 0.25 -bin ${BASE}${reg}_MNI_3mm.nii.gz
rm ${BASE}${reg}_thr_mask.nii.gz
done
