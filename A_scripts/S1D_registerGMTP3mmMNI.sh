#!/bin/bash

regions="avg152T1_csf avg152T1_white"

BASE='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/A_standards/tissuepriors/'
REF='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/A_standards/2009c_brain_3mm_str.nii'

for reg in $regions; do
flirt -in ${BASE}${reg}.nii -ref ${REF} -applyxfm -usesqform -out ${BASE}${reg}_thr_mask.nii.gz
fslmaths ${BASE}${reg}_thr_mask.nii.gz -thrp 35 -bin ${BASE}${reg}_MNI_3mm.nii.gz
rm ${BASE}${reg}_thr_mask.nii.gz
done

# create more conservative (i.e. eroded tissue priors for WM & CSF)

regions="avg152T1_csf avg152T1_white"

BASE='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/A_standards/tissuepriors/'
REF='/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/A_standards/2009c_brain_3mm_str.nii'

for reg in $regions; do
flirt -in ${BASE}${reg}.nii -ref ${REF} -applyxfm -usesqform -out ${BASE}${reg}_thr_mask.nii.gz
fslmaths ${BASE}${reg}_thr_mask.nii.gz -thrp 95 -bin ${BASE}${reg}_MNI_3mm.nii.gz
rm ${BASE}${reg}_thr_mask.nii.gz
done
