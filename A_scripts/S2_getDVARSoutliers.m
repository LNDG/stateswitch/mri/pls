% get DVARS output
% set a threshold at 10% of DeltapDvar values (Nichols paper suggests that adapting thresholds may be necessary)

% exclude 2131 & 2237 (r3 unfinished, r4 missing)
% N = 43 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

pn.DVARS = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/preproc/B_data/D_preproc/';

thresholds = [5];

% remove outliers at threshold 5 or 10% of largest DVARS volumes

SignificantDVARS = []; %SignificantDVARS_meanThresh = [];
for indID = 1:numel(IDs)
    disp(['Processing ', IDs{indID}])
    for indRun = 1:4
        try
            dataFile = [pn.DVARS, IDs{indID}, '/preproc2/run-',num2str(indRun),'/',IDs{indID}, '_run-',num2str(indRun),'_DVARS_tool.mat'];
            DVARSdata = load(dataFile);
            for indThresh = 1:numel(thresholds)
                normalSignificance  = find(DVARSdata.DVARS_Stat.pvals<0.05./(DVARSdata.DVARS_Stat.dim(2)-1));
                signDVARS = find(DVARSdata.DVARS_Stat.DeltapDvar>thresholds(indThresh));
                signDVARS = intersect(normalSignificance,signDVARS); %only if they are statistically sig as well!
                [sortVal, sortInd] = sort(DVARSdata.DVARS_Stat.DeltapDvar(:),'descend');
                if numel(signDVARS) > floor(length(sortVal)*0.1)
                    SignificantDVARS{indID,indRun,indThresh} = sortInd(1:ceil(length(sortVal)*0.1))+1;
                else
                    SignificantDVARS{indID,indRun,indThresh} = signDVARS+1;
                end
                %SignificantDVARS_meanThresh(indID,indRun,indThresh) = mean(DVARSdata.DVARS_Stat.DeltapDvar(find(DVARS_Stat.DeltapDvar >= thresholds(indThresh))));
            end
        catch
            disp('Error with analysis');
            %SignificantDVARS_meanThresh(indID,indRun,1:numel(thresholds)) = NaN;
        end
    end
end

DVARSout.IDs = IDs;
DVARSout.SignificantDVARS = SignificantDVARS;

save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/VoxelOverlap/S2_DVARS.mat'], 'DVARSout')

SignificantDVARS_num = cellfun(@numel, SignificantDVARS);

figure; hold on;
%histogram(SignificantDVARS_num(:,:,1), 20)
histogram(SignificantDVARS_num(:,:,2), 20)
% histogram(SignificantDVARS_num(:,:,3), 20)
% histogram(SignificantDVARS_num(:,:,4), 20)
xlabel('Number of deleted volumes')
ylabel('Number of runs/subjects')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% create a binary matrix of significant DVARS values
% subject*run*timepoints

indThresh = 1;
for indID = 1:numel(IDs)
    for indRun = 1:4
        curDVARS = SignificantDVARS{indID,indRun,indThresh};
        curBinary = zeros(1,1054);
        curBinary(curDVARS) = 1;
        DVARSbinaryMat(indID,indRun,:) = curBinary;
    end
end

figure;
plot(squeeze(nanmean(nanmean(DVARSbinaryMat,2),1)))

%% re-align DVARS according to condition onsets

addpath('/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc')

CompSignal = [];
for indID = 1:numel(IDs)
    for indRun = 1:4
        taskStructure = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/',IDs{indID},'_Run',num2str(indRun),'_regressors.mat']);
        for indLoad = 1:4
            tmpTime = find(sum(taskStructure.Regressors(:,7:10),2)==indLoad & ismember(taskStructure.Regressors(:,6),[2:7])); % only take sequence positions 2 through 7 here
            tmpSignal = [];
            for indStim = 1:numel(tmpTime)
                tmpSignal(indStim,:) = squeeze(DVARSbinaryMat(indID,indRun,tmpTime(indStim)-4-3:tmpTime(indStim)-4+25));
            end
            CompSignal(indID,indLoad,indRun,:) = nanmean(tmpSignal,1);
        end
    end
end

CompSignal = squeeze(nanmean(CompSignal,3));

time = [-3:25]*.645;
h = figure; hold on;
    thresh = 5; xl = [0 1]; p1 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.8 .8 .8]); p1.EdgeColor = 'none';p1.FaceAlpha = .3;
    thresh = 5; xl = [1 3]; p2 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.7 .7 .7]); p2.EdgeColor = 'none';p2.FaceAlpha = .3;
    thresh = 5; xl = [3 6]; p3 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.6 .6 .6]); p3.EdgeColor = 'none';p3.FaceAlpha = .3;
    thresh = 5; xl = [6 8]; p4 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.5 .5 .5]); p4.EdgeColor = 'none';p4.FaceAlpha = .3;
    thresh = 5; xl = [8 10]; p5 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[1 .3 .3]); p5.EdgeColor = 'none';p5.FaceAlpha = .3;
    thresh = 5; xl = [10 11]; p1 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.8 .8 .8]); p1.EdgeColor = 'none';p1.FaceAlpha = .3;
    thresh = 5; xl = [11 13]; p2 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.7 .7 .7]); p2.EdgeColor = 'none';p2.FaceAlpha = .3;
    thresh = 5; xl = [13 16]; p3 = patch([xl(1),xl(1),xl(2),xl(2)],[thresh,-thresh,-thresh,thresh],[.6 .6 .6]); p3.EdgeColor = 'none';p3.FaceAlpha = .3;
    cBrew = [.8 .8 .8; .6 .6 .6; .4 .4 .4; .2 .2 .2];
    % plot error bars
    for indCond = 1:4
        condAvg = squeeze(nanmean(CompSignal(:,:,:),2));
        curData = squeeze(nanmean(CompSignal(:,indCond,:),2));
        curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
            dimPlot{indCond} = shadedErrorBar(time,nanmean(curData,1),standError, ...
                'lineprops', {'color', cBrew(indCond,:),'linewidth', 2}, 'patchSaturation', .25);
    end
legend({'Cue'; 'Fix'; 'Stim'; 'Probe'; 'ITI'}, 'orientation', 'horizontal', 'location', 'SouthWest'); legend('boxoff')
title({['DVARS outliers (non-HRF-shifted)']})
xlabel('Time in seconds'); ylabel('Relative amount of outliers')
xlim([0, 25*.645]); ylim([0 .1])
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/C_figures/';
if ~isdir(pn.plotFolder); mkdir(pn.plotFolder); end
figureName = ['S2_DVARSoutlierDistribution'];
saveas(h, [pn.plotFolder, figureName], 'png');
close(h)
